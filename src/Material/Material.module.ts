import { Module } from '@nestjs/common';
import { MaterialService } from './Material.service';
import { MatrialController } from './Material.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Material } from './entities/Material.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Material])],
  controllers: [MatrialController],
  providers: [MaterialService],
})
export class MaterialModule {}
