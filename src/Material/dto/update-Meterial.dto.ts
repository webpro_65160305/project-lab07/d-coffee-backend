import { PartialType } from '@nestjs/swagger';
import { CreateMaterialDto } from './create-Material.dto';

export class UpdateMaterialDto extends PartialType(CreateMaterialDto) {}
