import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { MaterialService } from './Material.service';
import { CreateMaterialDto } from './dto/create-Material.dto';
import { UpdateMaterialDto } from './dto/update-Meterial.dto';

@Controller('materials')
export class MatrialController {
  constructor(private readonly materialService: MaterialService) {}
  // Create
  @Post()
  create(@Body() createMaterialDto: CreateMaterialDto) {
    return this.materialService.create(createMaterialDto);
  }
  // Read All
  @Get()
  findAll() {
    return this.materialService.findAll();
  }
  // Read One
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.materialService.findOne(+id);
  }
  // Partial Update
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateMaterialDto: UpdateMaterialDto,
  ) {
    return this.materialService.update(+id, updateMaterialDto);
  }
  // Delete
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.materialService.remove(+id);
  }
}
