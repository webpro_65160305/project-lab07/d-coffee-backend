import { Test, TestingModule } from '@nestjs/testing';
import { MatrialController } from './Material.controller';
import { MaterialService } from './Material.service';

describe('MatrialController', () => {
  let controller: MatrialController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MatrialController],
      providers: [MaterialService],
    }).compile();

    controller = module.get<MatrialController>(MatrialController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
