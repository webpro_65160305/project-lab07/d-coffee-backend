import { Injectable } from '@nestjs/common';
import { CreateMaterialDto } from './dto/create-Material.dto';
import { UpdateMaterialDto } from './dto/update-Meterial.dto';
import { Material } from './entities/Material.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MaterialService {
  constructor(
    @InjectRepository(Material)
    private materialRepository: Repository<Material>,
  ) {}
  create(createMaterialDto: CreateMaterialDto): Promise<Material> {
    return this.materialRepository.save(createMaterialDto);
  }
  findAll() {
    return this.materialRepository.find();
  }
  findOne(id: number) {
    return this.materialRepository.findOneBy({ id: id });
  }

  async update(id: number, updateMaterialDto: UpdateMaterialDto) {
    await this.materialRepository.update(id, updateMaterialDto);
    const user = await this.materialRepository.findOneBy({ id });
    return user;
  }
  async remove(id: number) {
    const deleteUser = await this.materialRepository.findOneBy({ id });
    return this.materialRepository.remove(deleteUser);
  }
}
